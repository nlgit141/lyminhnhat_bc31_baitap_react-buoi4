import React, { Component } from 'react';

import { ThemeProvider } from 'styled-components';
import { Container } from './Component/Container';
import { Dropdown } from './Component/Dropdown';
import { Heading1, Heading2, Heading3, Heading4, Heading5 } from './Component/Heading';
import { ToDoListDarkTheme } from './Theme/ToDoListDarkTheme';
import { ToDoListLightTheme } from './Theme/ToDoListLightTheme';
import { ToDoListPrimaryTheme } from './Theme/ToDoListPrimaryTheme';
import { TextField } from './Component/TextField';
import { arrTheme } from "./Theme/ThemeManager";

import { Button } from './Component/Button';
import { Table, Thead, Tr, Th } from './Component/Table';
import { connect } from 'react-redux';
import { adđTaskAction, changeThemeAction, doneTaskAction, deleteTaskAction, editTaskAction } from '../../redux/action/actionToDoList';



class ToDoList extends Component {
    state = {
        taskName: ""
    };
    renderTaskToDo = () => {
        return this.props.taskList.filter(task => !task.done).map((task, index) => {
            return (<Tr key={index}>
                <Th>{task.taskName}</Th>
                <Th className='text-right'>
                    <Button onClick={() => {
                        this.props.dispatch(editTaskAction(task));
                    }}><i className='fa fa-edit'></i></Button>

                    <Button onClick={() => {
                        this.props.dispatch(deleteTaskAction(task.id));
                    }}><i className='fa fa-trash'></i></Button>

                    <Button onClick={() => {
                        this.props.dispatch(doneTaskAction(task.id));
                    }}><i className='fa fa-check'></i></Button>
                </Th>
            </Tr>);
        });
    };


    renderTaskCompleted = () => {
        return this.props.taskList.filter(task => task.done).map((task, index) => {
            return (<Tr key={index}>
                <Th>{task.taskName}</Th>
                <Th className='text-right'>
                    <Button onClick={() => {
                        this.props.dispatch(deleteTaskAction(task.id));
                    }}><i className='fa fa-trash'></i></Button>
                </Th>
            </Tr>);
        });
    };
    renderTheme = () => {
        return arrTheme.map((theme, index) => {
            return <option key={index} value={theme.id}>{theme.name}</option>;
        });
    };

    render() {
        return (
            <div>
                <ThemeProvider theme={this.props.themeToDoList}>
                    <Container className='w-50'>
                        <Dropdown onChange={(e) => {
                            let { value } = e.target;
                            //Dispatch value lên reducer
                            this.props.dispatch(changeThemeAction(value));
                        }}>
                            {this.renderTheme()}
                        </Dropdown>
                        <Heading3>To do list</Heading3>
                        <TextField value={this.props.taskEdit.taskName} onChange={(e) => {
                            this.setState({
                                taskName: e.target.value
                            });
                        }} label="task name"></TextField>
                        <Button onClick={() => {
                            // lấy thông tin người dùng nhập vào input
                            let { taskName } = this.state;
                            //tạo ra 1 object task
                            let newTask = {
                                id: Date.now(),
                                taskName: taskName,
                                done: false
                            };
                            // đưa task object lên redux thông qua phương thức dispatch
                            this.props.dispatch(adđTaskAction(newTask));
                        }} className='ml-2'><i className='fa fa-plus'></i> Add Task</Button>
                        <Button className='ml-2'><i className='fa fa-upload'></i> update task</Button>
                        <hr />
                        <Heading3>Task to do</Heading3>
                        <Table>
                            <Thead>
                                {this.renderTaskToDo()}
                            </Thead>
                        </Table>
                        <Heading3>Task completed</Heading3>
                        <Table>
                            <Thead>
                                {this.renderTaskCompleted()}
                            </Thead>
                        </Table>
                    </Container>
                </ThemeProvider>
            </div>
        );
    }
};

const mapStaeToProps = state => {
    return {

        themeToDoList: state.ToDoListReducers.themeToDoList,
        taskList: state.ToDoListReducers.taskList,
        taskEdit: state.ToDoListReducers.taskEdit
    };
};

export default connect(mapStaeToProps)(ToDoList);