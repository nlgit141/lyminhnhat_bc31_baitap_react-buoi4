import { ToDoListDarkTheme } from "./ToDoListDarkTheme";
import { ToDoListLightTheme } from "./ToDoListLightTheme";
import { ToDoListPrimaryTheme } from "./ToDoListPrimaryTheme";

export const arrTheme = [
    {
        id: 1,
        name: "DarkTheme",
        theme: ToDoListDarkTheme
    },
    {
        id: 2,
        name: "LightTheme",
        theme: ToDoListLightTheme
    },
    {
        id: 3,
        name: "PrimaryTheme",
        theme: ToDoListPrimaryTheme
    }
];