import { add_task, change_theme, done_task, delete_task, edit_task } from "../types/ToDoListType";


export const adđTaskAction = (newTask) => {
    return {
        type: add_task,
        newTask
    };
};
export const doneTaskAction = (taskId) => {
    return {
        type: done_task,
        taskId
    };
};

export const deleteTaskAction = (taskId) => {
    return {
        type: delete_task,
        taskId
    };
};
export const editTaskAction = (task) => ({
    type: edit_task,
    task
});



export const changeThemeAction = (themeId) => {
    return {
        type: change_theme,
        themeId
    };
};

