import { ToDoListDarkTheme } from "../../BaiTapStyleComponent/BaiTapToDoList/Theme/ToDoListDarkTheme";
import { arrTheme } from "../../BaiTapStyleComponent/BaiTapToDoList/Theme/ThemeManager";
import { add_task, change_theme, delete_task, done_task, edit_task } from "../types/ToDoListType";


const initalState = {
    themeToDoList: ToDoListDarkTheme,
    taskList: [
        { id: 'task-1', taskName: 'task 1', done: true },
        { id: 'task-2', taskName: 'task 2', done: false },
        { id: 'task-3', taskName: 'task 3', done: true },
        { id: 'task-4', taskName: 'task 4', done: false },
    ],
    taskEdit: { id: 'task-4', taskName: 'task 4', done: false },

};

export default (state = initalState, action) => {
    switch (action.type) {

        case add_task: {
            // kiểm tra rỗng
            if (action.newTask.taskName.trim() === "") {
                alert("Task name is required!");
                return { ...state };
            }
            // kiểm tra tồn tại
            let taskListUpdate = [...state.taskList];
            let index = taskListUpdate.findIndex(task => task.taskName === action.newTask.taskName);
            if (index !== -1) {
                alert("task name already exits");
            }
            taskListUpdate.push(action.newTask);
            state.taskList = taskListUpdate;
            return { ...state };
        }

        case done_task: {
            // console.log("object", action.taskId);
            // click vào button check  => dispatch lên action có taskId
            let taskListUpdate = [...state.taskList];
            // từ taskId tìm ra task đó ở vị trị nào trong mảng  tiến hành cập nhật lại thuộc tính done = true và cập nhật lại vào state của redux
            let index = taskListUpdate.findIndex(task => task.id === action.taskId);
            if (index !== -1) {
                taskListUpdate[index].done = true;
                return { ...state, taskList: taskListUpdate };
            }
        }

        case delete_task: {
            // let taskListUpdate = [...state.taskList];                                           //! cách 1
            // taskListUpdate = taskListUpdate.filter(task => task.id !== action.taskId);
            // return {
            //     ...state, taskList: taskListUpdate
            // };

            return { ...state, taskList: state.taskList.filter(task => task.id !== action.taskId) }; //! cách 2 viết rút gọn
        }


        case edit_task: {
            return { ...state, taskEdit: action.task };
        }




        case change_theme: {
            let theme = arrTheme.find(theme => theme.id == action.themeId);
            if (theme) {
                state.themeToDoList = { ...theme.theme };
                return { ...state };
            }
        }

        default:
            return state;
    }
};