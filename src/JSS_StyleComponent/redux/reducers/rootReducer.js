import { combineReducers } from "redux";
import ToDoListReducers from "./ToDoListReducers";


export const rootReducer = combineReducers({
    ToDoListReducers
});
